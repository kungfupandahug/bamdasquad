package com.pairpro.pairpromythu;

import com.pairpro.pairpromythu.api.Endpoint;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Scanner;

@RestController
public class AnimeController {

    private final static String BASEURL = "https://ghibliapi.herokuapp.com";

    @RequestMapping("/people")
    public ArrayList<String> displayPeople() {
        ArrayList<String> names = new ArrayList<>();
        String url = BASEURL + "/people";
        JSONArray arr = null;

        try {
            arr = new JSONArray(Endpoint.requestURL(url));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (arr == null) return null;

        for (int i = 0; i < arr.length(); i++) {
            JSONObject obj = new JSONObject(arr.get(i).toString());
            if (obj.has("name"))
                names.add(obj.get("name").toString());
        }

        return names;
    }

    @RequestMapping("/films")
    public ArrayList<String> displayFilms() {
        ArrayList<String> names = new ArrayList<>();
        String url = BASEURL + "/films";
        JSONArray arr = null;

        try {
            arr = new JSONArray(Endpoint.requestURL(url));
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (arr == null) return null;

        for (int i = 0; i < arr.length(); i++) {
            JSONObject obj = new JSONObject(arr.get(i).toString());
            if (obj.has("title"))
                names.add(obj.get("title").toString());
        }

        return names;
    }

}
