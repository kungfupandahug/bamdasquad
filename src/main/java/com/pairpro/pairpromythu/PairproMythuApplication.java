package com.pairpro.pairpromythu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PairproMythuApplication {

	public static void main(String[] args) {
		SpringApplication.run(PairproMythuApplication.class, args);
	}

}
